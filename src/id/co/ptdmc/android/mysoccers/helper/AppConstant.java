package id.co.ptdmc.android.mysoccers.helper;

import java.util.Arrays;
import java.util.List;

public class AppConstant {

	// Number of columns of Grid View
	public static final int NUM_OF_COLUMNS = 3;

	// Gridview image padding
	public static final int GRID_PADDING = 8; // in dp

	// SD card image directory
	public static final String PHOTO_ALBUM = "NAT";

	// supported file formats
	public static final List<String> FILE_EXTN = Arrays.asList("jpg", "jpeg", "png");
	
	// Image resolution
	public static final String THUMBNAIL = "thumbnail";
	public static final String LOW_RESOLUTION = "low_resolution";
	public static final String STANDAR_RESOLUTION = "standard_resolution";
	
	// ACTION_IG
	public static final String ACTION_IG = "id.co.ptdmc.android.mysoccers.ACTION_IG";
	
	
}
