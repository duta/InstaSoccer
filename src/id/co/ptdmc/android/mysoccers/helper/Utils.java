package id.co.ptdmc.android.mysoccers.helper;

import id.co.ptdmc.android.mysoccers.R;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.WindowManager;

public class Utils {

	private static final String[] ids = {
		 "427553890",
		 "173560420",
		 "1094164909",
		 "637874562",
		 "26669533",
		 "183502050",
		 "566276178",
		 "1345521821",
		 "1470414259",
		 "704791681",
		 "246194371",
		 "185117603",
		 "299595863",
		 "1313063922",
		 "963986569",
		 "302920492",
		 "54174342",
		 "175975796",
		 "306102403",
		 "374997026"
	};

	private static final String[] names = {
		 "@leomessi - Leo Messi",
		 "@cristiano - Cristiano Ronaldo",
		 "@waynerooney - Wayne Rooney",
		 "@iamzlatanibrahimovic - IAmZlatan",
		 "@neymarjr - Nj",
		 "@robbenoficial - Arjen Robben",
		 "@hazardeden_10 - Eden Hazard",
		 "@stevengerrard - Steven Gerrard",
		 "@luissuarez9 - Luis Suarez",
		 "@gianluigibuffon - Gianluigi Buffon",
		 "@garethbale11 - Gareth Bale",
		 "@paulpogba_6 - Paul Labile Pogba",
		 "@pirloficial - Andrea Pirlo",
		 "@d_degeaofficial - David De Gea",
		 "@_rl9 - Robert Lewandowski",
		 "@aguerosergiokun16 - Sergio Leonel Ag�ero",
		 "@thiagosilva_33 - Thiago Silva",
		 "@davidluiz_4 - David Luiz",
		 "@fr7_official - Franck Rib�ry",
		 "@official_pepe - Pepe"
	};
	
	
	
	
	private static final String[] wiki = {
		 "http://en.wikipedia.org/wiki/Lionel_Messi",
		 "http://en.wikipedia.org/wiki/Cristiano_Ronaldo",
		 "http://en.wikipedia.org/wiki/Wayne_Rooney",
		 "http://en.wikipedia.org/wiki/Zlatan_Ibrahimovi%C4%87",
		 "http://en.wikipedia.org/wiki/Neymar",
		 "http://en.wikipedia.org/wiki/Arjen_Robben",
		 "http://en.wikipedia.org/wiki/Eden_Hazard",
		 "http://en.wikipedia.org/wiki/Steven_Gerrard",
		 "http://en.wikipedia.org/wiki/Luis_Su%C3%A1rez",
		 "http://en.wikipedia.org/wiki/Gianluigi_Buffon",
		 "http://en.wikipedia.org/wiki/Gareth_Bale",
		 "http://en.wikipedia.org/wiki/Paul_Pogba",
		 "http://en.wikipedia.org/wiki/Andrea_Pirlo",
		 "http://en.wikipedia.org/wiki/David_de_Gea",
		 "http://en.wikipedia.org/wiki/Robert_Lewandowski",
		 "http://en.wikipedia.org/wiki/Sergio_Ag%C3%BCero",
		 "http://en.wikipedia.org/wiki/Thiago_Silva",
		 "http://en.wikipedia.org/wiki/David_Luiz",
		 "http://en.wikipedia.org/wiki/Franck_Rib%C3%A9ry",
		 "http://en.wikipedia.org/wiki/Pepe_%28footballer,_born_1983%29"
	 };

	private static final String[] descriptions = {
		 "Bienvenidos a la cuenta oficial de Instagram de Leo Messi / Welcome to the official Leo Messi Instagram account http://www.leomessi.com/",
		 "http://www.facebook.com/cristiano",
		 "Official instagram account of Wayne Rooney. Manchester United & England captain http://www.waynerooney.com",
		 "Official Instagram of Swedish footballer Zlatan Ibrahimovic managed by Zlatan & his management http://www.facebook.com/zlatanibrahimovic",
		 "Pra quem tem pensamento forte, o imposs�vel � quest�o de opini�o !!!",
		 "FC Bayern | #10",
		 "Chelsea FC Follow me on twitter @hazardeden10",
		 "Official Steven Gerrard Instagram account. Liverpool FC Captain.",
		 "Cuenta oficial del jugador del FC Barcelona. http://www.luissuarez16.com",
		 "http://www.gianluigibuffon.it",
		"Footballer for @realmadrid and Wales http://www.facebook.com/Bale",
		"Football player of Juventus FC . http://www.twitter.com/paulpogba",
		"Bienvenuti alla mia pagina. Calciatore della Juventus FC.",
		"Official account of @manchesterunited 's player / Cuenta oficial del jugador del @manchesterunited",
		"Welcome to the OFFICIAL Instagram page of Robert Lewandowski | #rl9 | http://www.lewandowskiunlimited.com",
		"Argentino. Jugador del Manchester City. @aguerosergiokun www.facebook.com/sergioelkunaguero http://www.sergioaguero.com",
		"Assessoria Cadu Machado 021981337676",
		"God bless you! #geezers Twitter: @davidluiz_4 http://www.facebook.com/DavidLuizOfficial",
		"Franck Rib�ry",
		"Pepe"
	 };

	private static final Integer[] photos = {
		 R.drawable.pic1,
		 R.drawable.pic2,
		 R.drawable.pic3,
		 R.drawable.pic4,
		 R.drawable.pic5,
		 R.drawable.pic6,
		 R.drawable.pic7,
		 R.drawable.pic8,
		 R.drawable.pic9,
		 R.drawable.pic10,
		 R.drawable.pic11,
		 R.drawable.pic12,
		 R.drawable.pic13,
		 R.drawable.pic14,
		 R.drawable.pic15,
		 R.drawable.pic16,
		 R.drawable.pic17,
		 R.drawable.pic18,
		 R.drawable.pic19,
		 R.drawable.pic20,
	 };

	private Context _context;
	
	private String nextUrlImage;
	
	private List<String> standardResImages;

	public Utils(Context context) {
		this._context = context;
		this.standardResImages = new ArrayList<>();
	}

	/*
	 * getting screen width
	 */
	@SuppressWarnings("deprecation")
	public int getScreenWidth() {
		int columnWidth;
		WindowManager wm = (WindowManager) _context
				.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();

		final Point point = new Point();
		try {
			display.getSize(point);
		} catch (java.lang.NoSuchMethodError ignore) { // Older device
			point.x = display.getWidth();
			point.y = display.getHeight();
		}
		columnWidth = point.x;
		return columnWidth;
	}
	
	public String[] idUsers() {
		return ids;
	}
	
	public String[] nameUsers() {
		return names;
	}
	
	public String[] wikiUsers() {
		return wiki;
	}

	public String[] descriptionUsers() {
		return descriptions;
	}
	
	public Integer[] photoUsers() {
		return photos;
	}
   
	public List<String> getListImages(String id, String resolution) {
		List<String> listImageUrl = new ArrayList<String>();
		try {
			String iToken = _context.getString(R.string.client_token);
			URL url = new URL("https://api.instagram.com/v1/users/" + id
					+ "/media/recent/?access_token=" + iToken + "&COUNT=100");

			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

			// JSON data
			JSONObject jsonObject = new JSONObject(sb.toString());

			// JSON data pagination
			nextUrlImage = jsonObject.getJSONObject("pagination").getString("next_url");

			// JSON data image
			JSONArray jsonArray = jsonObject.getJSONArray("data");
			for (int i = 0; i < jsonArray.length(); i++) {
				String imageUrl = jsonArray.getJSONObject(i)
						.getJSONObject("images")
						.getJSONObject("" + resolution + "").getString("url");
				String standardImageUrl = jsonArray.getJSONObject(i)
						.getJSONObject("images")
						.getJSONObject("" + AppConstant.STANDAR_RESOLUTION + "").getString("url");
				listImageUrl.add(imageUrl);
				standardResImages.add(standardImageUrl);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return listImageUrl;
	}

	public List<String> getNextImages(String resolution) {
		List<String> listImageUrl = new ArrayList<String>();
		try {
			URL url = new URL(nextUrlImage);

			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

			// JSON data
			JSONObject jsonObject = new JSONObject(sb.toString());

			// JSON data pagination
			nextUrlImage = jsonObject.getJSONObject("pagination").getString("next_url");

			// JSON data image
			JSONArray jsonArray = jsonObject.getJSONArray("data");
			for (int i = 0; i < jsonArray.length(); i++) {
				String imageUrl = jsonArray.getJSONObject(i)
						.getJSONObject("images")
						.getJSONObject("" + resolution + "").getString("url");
				String standardImageUrl = jsonArray.getJSONObject(i)
						.getJSONObject("images")
						.getJSONObject("" + AppConstant.STANDAR_RESOLUTION + "").getString("url");
				listImageUrl.add(imageUrl);
				standardResImages.add(standardImageUrl);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return listImageUrl;
	}

	/**
	 * @return the standardResImages
	 */
	public List<String> getStandardResImages() {
		return standardResImages;
	}

}
