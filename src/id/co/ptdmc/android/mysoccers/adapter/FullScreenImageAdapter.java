package id.co.ptdmc.android.mysoccers.adapter;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import id.co.ptdmc.android.mysoccers.R;
import id.co.ptdmc.android.mysoccers.helper.BitmapCacheUtil;
import id.co.ptdmc.android.mysoccers.helper.TouchImageView;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
 
public class FullScreenImageAdapter extends PagerAdapter {
    
   	public final List<String> mThumbIds;
	
    private Activity _activity;
    
    private LayoutInflater inflater;
    
    private  AsyncTask<String, String, Bitmap> task;
 
    // Constructor
    public FullScreenImageAdapter(Activity activity, List<String> listImages){
        this._activity = activity;
        this.mThumbIds = listImages;
    }
 
    @Override
    public int getCount() {
        return mThumbIds.size();
    }
    
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }
    
    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {
        final TouchImageView imgDisplay;
  
        inflater = (LayoutInflater) _activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View viewLayout = inflater.inflate(R.layout.layout_full_image, container,
                false);
  
        imgDisplay = (TouchImageView) viewLayout.findViewById(R.id.full_image_view);
        
        task = new AsyncTask<String, String, Bitmap>() {
            @Override
            protected Bitmap doInBackground(final String... params) {
                BitmapCacheUtil cache = BitmapCacheUtil.getInstance();
                Bitmap bitmap = cache.getBitmapFromCache(mThumbIds.get(position));
                if (bitmap == null) {
	            	 try {
	            		 BitmapFactory.Options options = new BitmapFactory.Options();
	        	         options.inPreferredConfig = Bitmap.Config.ARGB_8888;
	
	                     URL url = new URL(mThumbIds.get(position));
	                     InputStream stream = url.openConnection().getInputStream();
	                     bitmap = BitmapFactory.decodeStream(stream); 
	                     cache.addBitmapToCache(mThumbIds.get(position), bitmap);
	                 } catch (MalformedURLException e) {
	                     e.printStackTrace();
	                 } catch (IOException e) {
	                     e.printStackTrace();
	                 }
                }
                return bitmap;
            }

            @Override
            protected void onPostExecute(final Bitmap bitmap) {
            	imgDisplay.setImageBitmap(bitmap); 
                ((ViewPager) container).addView(viewLayout);
            }
        };

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            task.execute();
        }    
  
        return viewLayout;
    }
    
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);
  
    }
 
}
