package id.co.ptdmc.android.mysoccers.adapter;

import id.co.ptdmc.android.mysoccers.WikiActivity;
import id.co.ptdmc.android.mysoccers.R;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

@SuppressLint("InflateParams")
public class MainContentAdapter extends ArrayAdapter<String> {
	
	private final Activity context;
	private final String[] itemname;
	private final String[] descript;
	private final String[] wiki;
	private final Integer[] imgid;
	 
	public MainContentAdapter(Activity context, String[] itemname, String[] descript, Integer[] imgid, String[] wikiSoccers) {
		super(context, R.layout.layout_main_content, itemname);

		this.wiki = wikiSoccers;
		this.context=context;
		this.itemname=itemname;
		this.descript=descript;
		this.imgid=imgid;
		
	}
	 
	@SuppressLint("ViewHolder")
	public View getView(final int position,View view,ViewGroup parent) {
		LayoutInflater inflater=context.getLayoutInflater();
		View rowView=inflater.inflate(R.layout.layout_main_content, null,true);
		
		TextView txtTitle = (TextView) rowView.findViewById(R.id.userSoccers);
		ImageView imageView = (ImageView) rowView.findViewById(R.id.photoSoccers);
		TextView extratxt = (TextView) rowView.findViewById(R.id.descriptSoccers);
		
		Button btnWiki = (Button) rowView.findViewById(R.id.button1);
		
		btnWiki.setOnClickListener(new View.OnClickListener() {
		    public void onClick(View v) {
		    	Intent intent = new Intent(context, WikiActivity.class);
				intent.putExtra("url", wiki[position]);
			    context.startActivity(intent);
		    }
		});
		
		txtTitle.setText(itemname[position]);
		imageView.setImageResource(imgid[position]);
		extratxt.setText(""+descript[position]);
		return rowView;
	};
	
	
	 
}
