package id.co.ptdmc.android.mysoccers.adapter;

import id.co.ptdmc.android.mysoccers.FullScreenViewActivity;
import id.co.ptdmc.android.mysoccers.helper.BitmapCacheUtil;
import id.co.ptdmc.android.mysoccers.helper.Utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class DetailContentAdapter extends BaseAdapter {

	private Activity _activity;
	private List<String> _filePaths;
	private int imageWidth;
	private String idUser;
	private Utils utils;

	public DetailContentAdapter(Activity activity, List<String> images,
			int imageWidth, String id, Utils utils) {
		this._activity = activity;
		this._filePaths = images;
		this.imageWidth = imageWidth;
		this.idUser = id;
		this.utils = utils;
	}

	@Override
	public int getCount() {
		return _filePaths.size();
	}

	@Override
	public Object getItem(int position) {
		return _filePaths.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		
		final ImageView imageView;
		
		if (convertView == null) {
			imageView = new ImageView(_activity);
		} else {
			imageView = (ImageView) convertView;
		}
		
		AsyncTask<String, String, Bitmap> task = new AsyncTask<String, String, Bitmap>() {
            @Override
            protected Bitmap doInBackground(final String... params) {
                BitmapCacheUtil cache = BitmapCacheUtil.getInstance();
                Bitmap bitmap = cache.getBitmapFromCache(_filePaths.get(position));
                if (bitmap == null) {
	            	 try {
	                     URL url = new URL(_filePaths.get(position));
	                     InputStream stream = url.openConnection().getInputStream();
	                     bitmap = BitmapFactory.decodeStream(stream);
	                     cache.addBitmapToCache(_filePaths.get(position), bitmap);
	                 } catch (MalformedURLException e) {
	                     e.printStackTrace();
	                 } catch (IOException e) {
	                     e.printStackTrace();
	                 }
	            }
                return bitmap;
            }

            @Override
            protected void onPostExecute(final Bitmap bitmap) {
            	imageView.setImageBitmap(bitmap);
            	imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        		imageView.setLayoutParams(new GridView.LayoutParams(imageWidth,
        				imageWidth));
        		imageView.setOnClickListener(new OnImageClickListener(position));
            }
        };

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            task.execute();
        }
		
		return imageView;
	}

	class OnImageClickListener implements OnClickListener {

		int _postion;

		// constructor
		public OnImageClickListener(int position) {
			this._postion = position;
		}

		@Override
		public void onClick(View v) {
			// on selecting grid view image
			// launch full screen activity
			Intent i = new Intent(_activity, FullScreenViewActivity.class);
			i.putExtra("position", _postion);
			i.putExtra("id", idUser);
			i.putStringArrayListExtra("list", (ArrayList<String>) utils.getStandardResImages());
			_activity.startActivity(i);
		}

	}

	/*
	 * Resizing image size
	 */
	public static Bitmap decodeFile(String filePath, int WIDTH, int HIGHT) {
		try {

			File f = new File(filePath);

			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new FileInputStream(f), null, o);

			final int REQUIRED_WIDTH = WIDTH;
			final int REQUIRED_HIGHT = HIGHT;
			int scale = 1;
			while (o.outWidth / scale / 2 >= REQUIRED_WIDTH
					&& o.outHeight / scale / 2 >= REQUIRED_HIGHT)
				scale *= 2;

			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void addMoreData(List<String> data) {
		_filePaths.addAll(data);
		notifyDataSetChanged();
	}

}
