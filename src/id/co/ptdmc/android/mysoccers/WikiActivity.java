package id.co.ptdmc.android.mysoccers;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ProgressBar;

public class WikiActivity extends ActionBarActivity {
	
	private EditText field;
	private WebView browser;
	private WebView web;
	private ProgressBar progress;
	
	
	ProgressBar progressBar;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail_soccers);
		
		Intent intent = getIntent();
		String url = intent.getStringExtra("url");
		
		//web = (WebView) findViewById(R.id.webView1);
		
		/*browser = (WebView)findViewById(R.id.webView1);
		browser.setWebViewClient(new MyBrowser());
		
		progress = (ProgressBar) findViewById(R.id.progressBar);
		//progress.setVisibility(View.GONE);
		
		
		web.setWebViewClient(new myWebClient());
        web.getSettings().setJavaScriptEnabled(true);
	    //myWebView.loadUrl("http://www.google.com");
		web.loadUrl(url);*/
		
		web = (WebView) findViewById(R.id.webView1);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        
        web.setWebViewClient(new myWebClient());
        web.getSettings().setJavaScriptEnabled(true);
        web.loadUrl(url);
	}
	
	 public class myWebClient extends WebViewClient
	    {
	    	@Override
	    	public void onPageStarted(WebView view, String url, Bitmap favicon) {
	    		// TODO Auto-generated method stub
	    		super.onPageStarted(view, url, favicon);
	    	}
	    	
	    	@Override
	    	public boolean shouldOverrideUrlLoading(WebView view, String url) {
	    		// TODO Auto-generated method stub
	    		
	    		view.loadUrl(url);
	    		return true;
	    		
	    	}
	    	
	    	@Override
	    	public void onPageFinished(WebView view, String url) {
	    		// TODO Auto-generated method stub
	    		super.onPageFinished(view, url);
	    		
	    		progressBar.setVisibility(View.GONE);
	    	}
	    }
	 
	
	private class MyBrowser extends WebViewClient {
	      @Override
	      public boolean shouldOverrideUrlLoading(WebView view, String url) {
	         view.loadUrl(url);
	         return true;
	      }
	   }

		
		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			// Inflate the menu; this adds items to the action bar if it is present.
			getMenuInflater().inflate(R.menu.detail_soccers, menu);
			return true;
		}

		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			// Handle action bar item clicks here. The action bar will
			// automatically handle clicks on the Home/Up button, so long
			// as you specify a parent activity in AndroidManifest.xml.
			int id = item.getItemId();
			//if (id == R.id.action_settings) {
			//	return true;
			//}
			return super.onOptionsItemSelected(item);
		}
	
}
/*public class WikiActivity extends Activity {
	
	private EditText field;
	private WebView browser;
	   
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail_soccers);
		
		browser = (WebView)findViewById(R.id.webView1);
		browser.setWebViewClient(new MyBrowser());
		
		WebView myWebView = (WebView) findViewById(R.id.webView1);
	      //myWebView.loadUrl("http://www.google.com");
	      myWebView.loadUrl("http://en.wikipedia.org/wiki/Cristiano_Ronaldo");
	}
	
	
	public void open(View view){
	      
      WebView myWebView = (WebView) findViewById(R.id.webView1);
      //myWebView.loadUrl("http://www.google.com");
      myWebView.loadUrl("http://en.wikipedia.org/wiki/Cristiano_Ronaldo");
	}
	
	private class MyBrowser extends WebViewClient {
	      @Override
	      public boolean shouldOverrideUrlLoading(WebView view, String url) {
	         view.loadUrl(url);
	         return true;
	      }
	   }

	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.detail_soccers, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}*/
