package id.co.ptdmc.android.mysoccers;

import id.co.ptdmc.android.mysoccers.adapter.MainContentAdapter;
import id.co.ptdmc.android.mysoccers.helper.Utils;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class MainActivity extends ActionBarActivity {
	
	private Utils utils;
	
	public String[] idSoccers;
	
	public String[] wikiSoccers;
	
	public String[] userSoccers;
	
	public String[] descriptSoccers;
	
	public Integer[] imgid;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		utils = new Utils(this);
		
		idSoccers = utils.idUsers();
		userSoccers = utils.nameUsers();
		wikiSoccers = utils.wikiUsers();
		descriptSoccers = utils.descriptionUsers();
		imgid = utils.photoUsers();
		
		MainContentAdapter adapter=new MainContentAdapter(this, userSoccers, descriptSoccers, imgid, wikiSoccers);
		ListView list = (ListView)findViewById(R.id.listSoccers);
		list.setAdapter(adapter);
		
		list.setOnItemClickListener(new OnItemClickListener() {
			 
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				 viewDetailSoccer(idSoccers[+position]);
			}
		 });
	}
	
	public void detailWikiOnClickHandler(View v) {
		
		Intent intent = new Intent(this, WikiActivity.class);
		intent.putExtra("url", "http://en.wikipedia.org/wiki/Cristiano_Ronaldo");
	    startActivity(intent);
	    
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
            Intent upIntent = NavUtils.getParentActivityIntent(this);
            if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                TaskStackBuilder.create(this).addNextIntentWithParentStack(upIntent).startActivities();
            } else {
                upIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(upIntent);
            }
            return true;
        }
		return super.onOptionsItemSelected(item);
	}
	
	
	   /**
	    * View detail soccer activity.
	    *
	    * @param post the post object required to view the details
	    * */
	   public void viewDetailSoccer(String id) {
	       Intent intent = new Intent(this, DetailActivity.class);
	       intent.putExtra("idUser", id);
	       startActivity(intent);
	   }
}
