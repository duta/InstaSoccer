package id.co.ptdmc.android.mysoccers;

import id.co.ptdmc.android.mysoccers.adapter.DetailContentAdapter;
import id.co.ptdmc.android.mysoccers.helper.AppConstant;
import id.co.ptdmc.android.mysoccers.helper.Utils;

import java.util.List;

import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.ProgressBar;

import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;


public class DetailActivity extends ActionBarActivity implements SwipyRefreshLayout.OnRefreshListener {
		
	private GridView gridView;
	 
	private Utils utils;
	
	private Bundle extras;
	
	private String userId;
	
	private DetailContentAdapter adapter;

	private int columnWidth;
	
	/**
     * SwipyRefreshLayout object.
     * */
    private SwipyRefreshLayout swipeLoadMore;
	
    private ProgressBar progressBarFoto;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail);
		
		gridView = (GridView) findViewById(R.id.grid_view);
		
		progressBarFoto = (ProgressBar) findViewById(R.id.progressBar);
		
		swipeLoadMore = (SwipyRefreshLayout) findViewById(R.id.swipeLoadMore);
		swipeLoadMore.setDirection(SwipyRefreshLayoutDirection.BOTTOM);
		swipeLoadMore.setOnRefreshListener(this);
		swipeLoadMore.setColorSchemeResources(
                R.color.red, R.color.orange, R.color.green, R.color.blue);
	}
	
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (adapter == null) {
        	utils = new Utils(this);
    		extras = getIntent().getExtras();
    		userId = extras.getString("idUser");
    		Log.i("LoadingInstagram","Loading");
    		// Initializing Grid View
    		InitializeGridLayout();
    		
    		// populate list with data
            populateData(userId);
            
        }
    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(final Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
            Intent upIntent = NavUtils.getParentActivityIntent(this);
            if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                TaskStackBuilder.create(this).addNextIntentWithParentStack(upIntent).startActivities();
            } else {
                upIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(upIntent);
            }
            return true;
        }
		return super.onOptionsItemSelected(item);
	}

    @Override
    protected void onActivityResult(
            final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        
    }
	
   /**
    * Populate data to display.
    *
    * @param id for id account instagram
    * */
   public void populateData(final String id) {
      
		AsyncTask<String, String, List<String>> task = new AsyncTask<String, String, List<String>>() {
           @Override
           protected List<String> doInBackground(final String... params) {
           		return utils.getListImages(id, AppConstant.THUMBNAIL);
           }

           @Override
           protected void onPostExecute(final List<String> listImages) {
	       		// Gridview adapter
	       		adapter = new DetailContentAdapter(DetailActivity.this, listImages, columnWidth, id, utils);
	       		// setting grid view adapter
	       		gridView.setAdapter(adapter);
	       		Log.i("LoadingInstagram","Finish");
	       		progressBarFoto.setVisibility(View.GONE);
           }
       };

       int currentapiVersion = android.os.Build.VERSION.SDK_INT;
       if (currentapiVersion >= android.os.Build.VERSION_CODES.HONEYCOMB) {
           task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
       } else {
           task.execute();
       }
       	
   }


	private void InitializeGridLayout() {
		Resources r = getResources();
		float padding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
				AppConstant.GRID_PADDING, r.getDisplayMetrics());

		columnWidth = (int) ((utils.getScreenWidth() - ((AppConstant.NUM_OF_COLUMNS + 1) * padding)) / AppConstant.NUM_OF_COLUMNS);

		gridView.setNumColumns(AppConstant.NUM_OF_COLUMNS);
		gridView.setColumnWidth(columnWidth);
		gridView.setStretchMode(GridView.NO_STRETCH);
		gridView.setPadding((int) padding, (int) padding, (int) padding,
				(int) padding);
		gridView.setHorizontalSpacing((int) padding);
		gridView.setVerticalSpacing((int) padding);
	}

	@Override
	public void onRefresh(SwipyRefreshLayoutDirection direction) {
		AsyncTask<String, String, List<String>> task = new AsyncTask<String, String, List<String>>() {
	           @Override
	           protected List<String> doInBackground(final String... params) {
	           		return utils.getNextImages(AppConstant.THUMBNAIL);
	           }

	           @Override
	           protected void onPostExecute(final List<String> listImages) {
		       		((DetailContentAdapter) gridView.getAdapter()).addMoreData(listImages);
		       		swipeLoadMore.setRefreshing(false);
	           }

			@Override
			protected void onCancelled() {
				swipeLoadMore.setRefreshing(false);
			}
	       };

	       int currentapiVersion = android.os.Build.VERSION.SDK_INT;
	       if (currentapiVersion >= android.os.Build.VERSION_CODES.HONEYCOMB) {
	           task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	       } else {
	           task.execute();
	       }
	}
}
